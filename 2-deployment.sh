kubectl create deployment --image=nginx nginx -n webinar-k8s
kubectl get all -n webinar-k8s
kubectl delete pod nginx-* -n webinar-k8s
kubectl scale deploy nginx --replicas=3 -n webinar-k8s
kubectl get deploy nginx -n webinar-k8s
