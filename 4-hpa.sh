kubectl edit deploy nginx -n webinar-k8s
# add requests: cpu: 0,01 
kubectl autoscale deployment nginx --cpu-percent=30 --min=1 --max=4 -n webinar-k8s
kubectl run -i --tty load-generator --rm --image=busybox --restart=Never -- /bin/sh -c "while sleep 0.01; do wget -q -O- http://aa3ea56c6a5c84c1caa58da74f5670f6-160694838.us-east-1.elb.amazonaws.com; done"
